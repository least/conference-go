import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_location_picture_url(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query=${city}, ${state}&per_page=1"
    r = requests.get(url, headers=headers)
    j = json.loads(r.content)
    picture_url = j["photos"][0]["src"]["original"]
    return picture_url

def get_weather_data(city, state):
    # headers = {'Authorization': OPEN_WEATHER_API_KEY}
    country_code = "US"
    url = f"https://api.openweathermap.org/data/2.5/weather?q={city},{state},{country_code}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    r = requests.get(url)
    j = json.loads(r.content)
    # print(j["main"]["temp"], j["weather"][0]["description"])
    if j["cod"] == "404":
        return None
    else:
        return {
                "temp": j["main"]["temp"],
                "description": j["weather"][0]["description"] 
                }
